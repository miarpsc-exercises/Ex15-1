# McD's more detailed thoughts

(This is pasted from an org file, so it doesn't come out so pretty in markdown)


* Organization

** Online via Vyew

** Meeting per district

* Objectives

** Green field operation

** Temporary SEOC

** NTS activation?

* Scenario

** Flood

** County has moved ops to backup EOC

** SEOC has moved to different district

   which depends on district being tested, select district opposite,
   e.g. for D5 SEOC will be in D3, for D6, D2, etc.

** What if county has other, random loc

* Issues

** Prepared for antennas, towers

** Available power, fuel, batteries

** HT spare batteries

** Low power modes; CW, Olivia

** Access to MI-CIMS

** Plan to activate NTS

*** Is NTS on board?

**** Does NTS know?

***** What if NM unavailable, backup?

***** Regular planning with NTS?

** What if net members also affected?




[back to home](Home)

