# Proposed schedule

- District 1 and K8AE - May 9

- District 2 and KD8QPF - May 23

- District 3 and WB8TQZ - May 9

- District 5 and WB8TQZ - Jun 13

- District 6 and K8RDN - May 30

- District 7 and N8FVM - May 30

- District 8 and WA8DHB - May 23

- NWS and WA8IAL - Jun 13

Some Districts may prefer a weeknight, and some may have significant events planned for these dates, so the dates will almost certainly change.

[Back to Home](Home)


