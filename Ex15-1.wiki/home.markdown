# Initial thoughts on 15-1

- By phone/web like 14-1
- Focus on portable operations
  -  local EOC in nostandard location
  -  SEOC in nostandard location
- Portable access to NTS
- Planning for
  -  Antennas
  -  Power, including fuel
  -  Access to MI-CIMS
  -  Access to NTS resources
  -  Creature comforts for extended deployment


[more details](org-file)

[draft schedule](schedule)

